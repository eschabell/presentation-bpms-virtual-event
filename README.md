Virtual Event JBoss BRMS & BPM Suite 6.1 Launch
-----------------------------------------------
View presentaion online:
[https://eschabell.gitlab.io/presentation-bpms-virtual-event](https://eschabell.gitlab.io/presentation-bpms-virtual-event)


![Cover Slide](cover.png)


Released versions
-----------------
- v1.0 - session delivered on April 13, 2015 for online virtual event.
